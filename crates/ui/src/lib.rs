use std::{
    cell::RefCell,
    path::PathBuf,
    rc::Rc,
    sync::{Arc, Mutex},
};

use aoide_desktop_app::settings;
use slint::ComponentHandle;

use moire_audio_dsp::engine as audio_engine;
use moire_audio_io::jack::{self, JackBackend};

pub use moire_library_backend::Backend as LibraryBackend;
pub use moire_library_frontend::{callback as library_callback, Frontend as LibraryFrontend};

use moire_ui_generated::MainWindow;

mod deck;
mod tasklet;

pub struct Gui {
    main_window: MainWindow,
    _tokio_rt: tokio::runtime::Handle,
    _library: Arc<Library>,
    _poll_audio_rx_timer: slint::Timer,
    _buffer_size_frames: Rc<RefCell<usize>>,
    _sample_rate: Rc<RefCell<usize>>,
    _to_audio_tx: Arc<Mutex<rtrb::Producer<audio_engine::Command>>>,
    _jack_backend: Rc<JackBackend>,
}

pub(crate) struct Library {
    backend: LibraryBackend,
    frontend: LibraryFrontend,
}

/// Gui is responsible for running the GUI after the rest of the
/// application has been initialized.
impl Gui {
    #[allow(clippy::too_many_arguments)] // FIXME
    pub fn new(
        config_dir: PathBuf,
        tokio_rt: tokio::runtime::Handle,
        library_backend: LibraryBackend,
        initial_settings: settings::State,
        to_audio_tx: rtrb::Producer<audio_engine::Command>,
        mut from_audio_rx: rtrb::Consumer<audio_engine::Event>,
        to_jack_tx: rtrb::Producer<jack::Command>,
        jack_backend: Rc<JackBackend>,
        gc_handle: basedrop::Handle,
    ) -> Gui {
        let main_window = MainWindow::new();

        let library_frontend =
            LibraryFrontend::spawn(config_dir, &tokio_rt, library_backend.handle());

        // Connect library backend properties (updated by tasklets) and callbacks

        tasklet::spawn_music_dir_changed_property_updater(
            main_window.as_weak(),
            &tokio_rt,
            &library_frontend,
        );
        main_window.on_lib_music_dir_choose(library_callback::on_music_dir_choose(
            tokio_rt.clone(),
            &library_frontend,
        ));
        main_window.on_lib_music_dir_reset(library_callback::on_music_dir_reset(&library_frontend));

        tasklet::spawn_collection_property_updater(
            main_window.as_weak(),
            &tokio_rt,
            &library_frontend,
        );
        main_window.on_lib_collection_rescan_music_dir(
            library_callback::on_collection_rescan_music_dir(
                tokio_rt.clone(),
                library_backend.handle().downgrade(),
                &library_frontend,
            ),
        );

        tasklet::spawn_track_search_property_updater(
            main_window.as_weak(),
            &tokio_rt,
            &library_frontend,
        );
        main_window.on_lib_track_search_submit_input({
            let mut on_track_search_submit_input =
                library_callback::on_track_search_submit_input(&library_frontend);
            move |input| on_track_search_submit_input(input.into()).into()
        });

        // Propagate the initial settings through the connected FRP network
        // after wiring everything together and before showing the UI.
        // This will also save the aoide settings persistently to disk.
        library_frontend.settings_state().modify(move |settings| {
            *settings = initial_settings;
            true
        });

        let library = Arc::new(Library {
            backend: library_backend,
            frontend: library_frontend,
        });

        let buffer_size_frames = Rc::new(RefCell::new(0));
        let sample_rate = Rc::new(RefCell::new(0));
        let to_audio_tx = Arc::new(Mutex::new(to_audio_tx));

        main_window.on_volume_changed_on_deck(deck::on_volume_changed(&to_audio_tx));
        main_window.on_tempo_changed_on_deck(deck::on_tempo_changed(&to_audio_tx));

        // The buffer size must be known before creating decks.
        // The initial BufferSizeChanged message comes from querying JACk before activating
        // the JACK client. Then, sometimes Pipewire calls the JACK buffer sized changed
        // callback before it starts calling the JACK process callback. In that case, there
        // will be two BufferSizeChanged callbacks in the channel, so this needs to loop.
        while let Ok(audio_engine::Event::BufferSizeChanged(_)) = from_audio_rx.peek() {
            if let Ok(audio_engine::Event::BufferSizeChanged(frames)) = from_audio_rx.pop() {
                let mut buffer_size_frames = buffer_size_frames.borrow_mut();
                *buffer_size_frames = frames;
            } else {
                unreachable!("audio_engine::Event not a BufferSizeChanged message?!")
            }
        }
        let mut add_deck_callback = deck::on_add_deck(
            &main_window,
            &buffer_size_frames,
            &to_audio_tx,
            to_jack_tx,
            &jack_backend,
            &gc_handle,
        );
        add_deck_callback();
        main_window.on_add_deck(add_deck_callback);

        main_window.on_load_file_to_deck(deck::on_load_file(
            &main_window,
            tokio_rt.clone(),
            &library,
            &buffer_size_frames,
            &sample_rate,
            &to_audio_tx,
            &gc_handle,
        ));

        main_window.on_clip_seek_on_deck(deck::on_clip_seek(&to_audio_tx));

        main_window.on_headphones_changed_on_deck(deck::on_headphones_changed(&to_audio_tx));

        let poll_audio_rx_timer = slint::Timer::default();
        poll_audio_rx_timer.start(
            slint::TimerMode::Repeated,
            std::time::Duration::from_millis(100),
            on_gui_poll_timer(
                &main_window,
                &buffer_size_frames,
                &sample_rate,
                from_audio_rx,
            ),
        );

        Gui {
            main_window,
            _tokio_rt: tokio_rt,
            _library: library,
            _poll_audio_rx_timer: poll_audio_rx_timer,
            _buffer_size_frames: buffer_size_frames,
            _sample_rate: sample_rate,
            _to_audio_tx: to_audio_tx,
            _jack_backend: jack_backend,
        }
    }

    pub fn run(&self) {
        self.main_window.run();
    }
}

fn on_gui_poll_timer(
    main_window: &MainWindow,
    buffer_size_frames: &Rc<RefCell<usize>>,
    sample_rate: &Rc<RefCell<usize>>,
    mut from_audio_rx: rtrb::Consumer<audio_engine::Event>,
) -> impl FnMut() {
    let main_window = main_window.as_weak();
    let buffer_size_frames = Rc::downgrade(buffer_size_frames);
    let sample_rate = Rc::downgrade(sample_rate);
    move || {
        let mut last_received_time = 0f32;
        while from_audio_rx.slots() > 0 {
            match from_audio_rx.pop().unwrap() {
                audio_engine::Event::TimeElapsed(t) => last_received_time = t,
                audio_engine::Event::BufferSizeChanged(frames) => {
                    let buffer_size_frames = buffer_size_frames.upgrade().unwrap();
                    let mut buffer_size_frames = buffer_size_frames.borrow_mut();
                    *buffer_size_frames = frames;
                }
                audio_engine::Event::SampleRateChanged(new_sample_rate) => {
                    let sample_rate = sample_rate.upgrade().unwrap();
                    let mut sample_rate = sample_rate.borrow_mut();
                    *sample_rate = new_sample_rate;
                }
            }
        }
        let main_window = if let Some(main_window) = main_window.upgrade() {
            main_window
        } else {
            return;
        };
        main_window.set_time(last_received_time);
        if last_received_time != 0f32 {
            let hours = (last_received_time / 3600f32).floor();
            let minutes = (last_received_time / 60f32).floor();
            let seconds = (last_received_time % 60f32).floor();
            let deciseconds =
                ((last_received_time - last_received_time.floor() as f32) * 10f32).floor();
            let string = format!("{}:{}:{}.{}", hours, minutes, seconds, deciseconds);
            main_window.set_duration_text(string.into());
        }
    }
}
