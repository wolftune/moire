use std::{
    cell::RefCell,
    rc::Rc,
    sync::{Arc, Mutex},
};

use basedrop::Owned;
use slint::{ComponentHandle, Model, ModelRc, SharedString, VecModel};

use aoide_core::track::EntityUid as TrackUid;
use std::str::FromStr;

use crate::Library;

use moire::waveform_analyzer::{WaveformAnalysis, WaveformAnalyzer};

use moire_audio_dsp::{engine as audio_engine, Clip as AudioClip, Deck as AudioDeck};
use moire_audio_io::jack::{self, JackBackend};

pub use moire_library_backend::Backend as LibraryBackend;
pub use moire_library_frontend::{callback as library_callback, Frontend as LibraryFrontend};

use moire_ui_generated::{ClipProps, DeckProps, MainWindow};

pub(crate) fn on_volume_changed(
    to_audio_tx: &Arc<Mutex<rtrb::Producer<audio_engine::Command>>>,
) -> impl FnMut(i32, f32) {
    let to_audio_tx = Arc::downgrade(to_audio_tx);
    move |deck_index, value| {
        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.lock().unwrap();
        to_audio_tx
            .push(audio_engine::Command::SetVolume {
                deck_index: deck_index as usize,
                value,
            })
            .expect("GUI to audio channel full!");
    }
}

pub(crate) fn on_tempo_changed(
    to_audio_tx: &Arc<Mutex<rtrb::Producer<audio_engine::Command>>>,
) -> impl FnMut(i32, f32) {
    let to_audio_tx = Arc::downgrade(to_audio_tx);
    move |deck_index, value| {
        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.lock().unwrap();
        to_audio_tx
            .push(audio_engine::Command::SetTempo {
                deck_index: deck_index as usize,
                value,
            })
            .expect("GUI to audio channel full!");
    }
}

pub(crate) fn on_add_deck(
    main_window: &MainWindow,
    buffer_size_frames: &Rc<RefCell<usize>>,
    to_audio_tx: &Arc<Mutex<rtrb::Producer<audio_engine::Command>>>,
    mut to_jack_tx: rtrb::Producer<jack::Command>,
    jack_backend: &Rc<JackBackend>,
    gc_handle: &basedrop::Handle,
) -> impl FnMut() {
    let main_window = main_window.as_weak();
    let buffer_size_frames = Rc::downgrade(buffer_size_frames);
    let to_audio_tx = Arc::downgrade(to_audio_tx);
    let gc_handle = gc_handle.clone();
    let jack_backend = Rc::downgrade(jack_backend);
    move || {
        let main_window = if let Some(main_window) = main_window.upgrade() {
            main_window
        } else {
            return;
        };
        let decks = main_window.get_decks();
        let deck_count = match decks.as_any().downcast_ref::<VecModel<DeckProps>>() {
            None => {
                let model = VecModel::from(vec![empty_deckdata()]);
                main_window.set_decks(ModelRc::new(model));
                1
            }
            Some(decks) => {
                decks.push(empty_deckdata());
                decks.row_count()
            }
        };

        let deck_index = deck_count;

        let jack_backend = jack_backend.upgrade().unwrap();
        let jack_client = jack_backend.client();
        let port_l = jack_client
            .register_port(&format!("deck{deck_index}_L"), Default::default())
            .unwrap();
        let port_r = jack_client
            .register_port(&format!("deck{deck_index}_R"), Default::default())
            .unwrap();
        let vec_l = Owned::new(&gc_handle, Vec::with_capacity(deck_count));
        let vec_r = Owned::new(&gc_handle, Vec::with_capacity(deck_count));
        to_jack_tx
            .push(jack::Command::AddDeck {
                port_l,
                port_r,
                vec_l,
                vec_r,
            })
            .unwrap();

        let mut new_deck = AudioDeck::default();
        let buffer_size_frames = *buffer_size_frames.upgrade().unwrap().borrow();
        new_deck.set_buffer_size(buffer_size_frames);

        let vec = Vec::<AudioDeck>::with_capacity(deck_count);

        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.lock().unwrap();
        to_audio_tx
            .push(audio_engine::Command::AddDeck {
                deck: new_deck,
                vec: Owned::new(&gc_handle, vec),
            })
            .expect("GUI to audio channel full!");
    }
}

pub(crate) fn on_load_file(
    main_window: &MainWindow,
    tokio_rt: tokio::runtime::Handle,
    library: &Arc<Library>,
    buffer_size_frames: &Rc<RefCell<usize>>,
    sample_rate: &Rc<RefCell<usize>>,
    to_audio_tx: &Arc<Mutex<rtrb::Producer<audio_engine::Command>>>,
    gc_handle: &basedrop::Handle,
) -> impl FnMut(i32, f32, SharedString) {
    let main_window = main_window.as_weak();
    let library = Arc::downgrade(library);
    let to_audio_tx = Arc::downgrade(to_audio_tx);
    let buffer_size_frames = Rc::downgrade(buffer_size_frames);
    let sample_rate = Rc::downgrade(sample_rate);
    let gc_handle = gc_handle.clone();
    move |index, time, track_uid| {
        let invoked_at = std::time::Instant::now();

        if track_uid.is_empty() {
            log::warn!("Deck {index}: No track selected for loading");
            return;
        }
        log::debug!("Deck {index}: Loading track '{track_uid}' from library");
        let track_uid = TrackUid::from_str(&track_uid).expect("valid track UID");

        let library = library.upgrade().unwrap();

        let main_window = main_window.clone();
        let to_audio_tx = to_audio_tx.clone();
        let gc_handle = gc_handle.clone();
        // TODO: Use atomics wrapped into an Arc for buffer size and sample rate if they
        // should be evaluated just before actually loading the track, i.e. after finishing
        // the asynchronous load from the library and re-entering the UI thread. Tunneling
        // them through the executor thread requires Send although they are only ever
        // accessed in the UI thread and never in the executor thread.
        // Until the requirements are clear simply read out their values now and accept that
        // they might change at any time before actually loading the track into the deck.
        let buffer_size_frames = *buffer_size_frames.upgrade().unwrap().borrow();
        let sample_rate = *sample_rate.upgrade().unwrap().borrow();
        tokio_rt.spawn(async move {
            let track_entity = if let Some(track_entity) = library
                    .frontend
                    .load_tracks_by_uid(library.backend.handle(), vec![track_uid.clone()]).await
            .map_err(|err| {
                log::error!("Deck {index}: Failed to load track '{track_uid}' from library: {err}");
            })
            .ok()
            .and_then(|v| v.into_iter().next())
            {
                track_entity
            } else {
                log::warn!("Deck {index}: Track '{track_uid}' not found in library");
                return;
            };

            let file_path = track_entity
                .body
                .content_url
                .as_ref()
                .and_then(|content_url| content_url.to_file_path().ok())
                .expect("local file URL");

            let elapsed_since_invoked = invoked_at.elapsed();
            log::debug!(
                "Deck {index}: Loading track '{file_path}' from library took {elapsed_since_invoked_millis} ms",
                file_path = file_path.display(),
                elapsed_since_invoked_millis = elapsed_since_invoked.as_millis(),
            );

            let mut waveform_analyzer = WaveformAnalyzer::new(&file_path);
            tokio::task::block_in_place(|| waveform_analyzer.analyze());
            let waveform = draw_waveform(waveform_analyzer.result());

            main_window.upgrade_in_event_loop(move |main_window| {
                // FIXME: Is the deck still supposed to load the track that has now
                // become available, i.e. is a load operation for the corresponding
                // track uid still pending? Otherwise we should abort this operation
                // and discard the loaded track! Finite state machines are inevitable
                // in asynchronous systems for predictable behavior.

                let elapsed_since_invoked = invoked_at.elapsed();
                let start_on_timeline_seconds = time + elapsed_since_invoked.as_secs_f32();

                let decks = main_window.get_decks();
                let decks = decks
                    .as_any()
                    .downcast_ref::<VecModel<DeckProps>>()
                    .unwrap();
                let deck = decks.row_data(index as usize).unwrap_or_default();


                let clips = deck
                    .clips
                    .as_any()
                    .downcast_ref::<VecModel<ClipProps>>()
                    .unwrap();
                clips.push(ClipProps {
                    start_on_timeline_seconds,
                    waveform,
                });

                let vec = Vec::with_capacity(clips.row_count());

                let to_audio_tx = to_audio_tx.upgrade().unwrap();
                let mut to_audio_tx = to_audio_tx.lock().unwrap();
                to_audio_tx
                    .push(audio_engine::Command::LoadClip {
                        deck_index: index as usize,
                        clip: AudioClip::new(
                            sample_rate,
                            buffer_size_frames,
                            &file_path,
                            start_on_timeline_seconds,
                            0.0,
                            None,
                        ),
                        vec: Owned::new(&gc_handle, vec),
                    })
                    .expect("GUI to audio channel full!");
            })
        });
    }
}

pub(crate) fn on_clip_seek(
    to_audio_tx: &Arc<Mutex<rtrb::Producer<audio_engine::Command>>>,
) -> impl FnMut(i32, i32, f32) {
    let to_audio_tx = Arc::downgrade(to_audio_tx);
    move |deck_index, clip_index, diff_seconds| {
        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.lock().unwrap();
        to_audio_tx
            .push(audio_engine::Command::MoveClip {
                deck_index: deck_index as usize,
                clip_index: clip_index as usize,
                diff_seconds,
            })
            .expect("GUI to audio channel full!");
    }
}

pub(crate) fn on_headphones_changed(
    to_audio_tx: &Arc<Mutex<rtrb::Producer<audio_engine::Command>>>,
) -> impl FnMut(i32, bool) {
    let to_audio_tx = Arc::downgrade(to_audio_tx);
    move |index, value| {
        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.lock().unwrap();
        to_audio_tx
            .push(audio_engine::Command::SetHeadphones {
                deck_index: index as usize,
                value,
            })
            .expect("GUI to audio channel full!");
    }
}

fn empty_deckdata() -> DeckProps {
    DeckProps {
        clips: ModelRc::new(VecModel::from(vec![])),
    }
}

fn draw_waveform(analysis_data: &WaveformAnalysis) -> slint::Image {
    let height = 200;
    let mut waveform_buffer =
        slint::SharedPixelBuffer::<slint::Rgba8Pixel>::new(analysis_data.len() as u32, height);
    let mut pixmap = tiny_skia::PixmapMut::from_bytes(
        waveform_buffer.make_mut_bytes(),
        analysis_data.len() as u32,
        height as u32,
    )
    .unwrap();
    pixmap.fill(tiny_skia::Color::WHITE);

    let path = {
        let mut path_builder = tiny_skia::PathBuilder::new();
        for (x, bin) in analysis_data.iter().enumerate() {
            path_builder.line_to(
                x as f32,
                height as f32 / 2.0 + bin.minimum * height as f32 / 2.0,
            );
            path_builder.line_to(
                x as f32,
                height as f32 / 2.0 + bin.maximum * height as f32 / 2.0,
            );
        }
        path_builder.finish().unwrap()
    };

    let mut paint = tiny_skia::Paint::default();
    paint.set_color_rgba8(0, 0, 255, 255);
    paint.anti_alias = true;

    pixmap.stroke_path(
        &path,
        &paint,
        &Default::default(),
        Default::default(),
        Default::default(),
    );

    slint::Image::from_rgba8_premultiplied(waveform_buffer)
}
