use symphonia::core::audio::{AudioBufferRef, SampleBuffer};
use symphonia::core::codecs::DecoderOptions;
use symphonia::core::formats::FormatOptions;
use symphonia::core::io::MediaSourceStream;
use symphonia::core::meta::MetadataOptions;
use symphonia::core::probe::Hint;

#[derive(Clone)]
pub struct WaveformBin {
    pub minimum: f32,
    pub maximum: f32,
}

pub type WaveformAnalysis = [WaveformBin];

pub struct WaveformAnalyzer<'a> {
    source: &'a std::path::Path,
    frame_in_track: usize,
    frames_per_bin: usize,
    result: Vec<WaveformBin>,
}

impl<'a> WaveformAnalyzer<'a> {
    pub fn new(source: &'a std::path::Path) -> WaveformAnalyzer<'a> {
        WaveformAnalyzer {
            source,
            result: Vec::<WaveformBin>::new(),
            frames_per_bin: 0,
            frame_in_track: 0,
        }
    }

    pub fn analyze(&mut self) {
        log::debug!(
            "Starting generation of waveform for {}",
            self.source.display()
        );

        let invoked_at = std::time::Instant::now();

        let file = Box::new(std::fs::File::open(&self.source).unwrap());
        let stream = MediaSourceStream::new(file, Default::default());
        let format_opts: FormatOptions = Default::default();
        let metadata_opts: MetadataOptions = Default::default();
        let decoder_opts: DecoderOptions = Default::default();

        let probe_result = symphonia::default::get_probe()
            .format(&Hint::new(), stream, &format_opts, &metadata_opts)
            .unwrap();

        let mut reader = probe_result.format;
        let codec_params = &reader.default_track().unwrap().codec_params;
        let mut decoder = symphonia::default::get_codecs()
            .make(codec_params, &decoder_opts)
            .unwrap();

        // All common audio sample rates (44100, 48000, 96000) * 0.2 (200 milliseconds)
        // equals an integer number of frames per bin.
        self.frames_per_bin = (codec_params.sample_rate.unwrap() as f32 * 0.2).floor() as usize;

        self.result.resize(
            (codec_params.n_frames.unwrap() as usize / self.frames_per_bin) + 1,
            WaveformBin {
                minimum: 0.0,
                maximum: 0.0,
            },
        );

        while let Ok(packet) = reader.next_packet() {
            let audio_buffer = decoder.decode(&packet).unwrap();
            self.process(audio_buffer);
        }

        log::debug!(
            "Generated waveform for {} in {} ms",
            self.source.display(),
            invoked_at.elapsed().as_millis()
        );
    }

    fn process(&mut self, buffer_ref: AudioBufferRef) {
        let frames = buffer_ref.frames();
        let channels = buffer_ref.spec().channels.count();

        let mut buffer = SampleBuffer::<f32>::new(
            buffer_ref.capacity().try_into().unwrap(),
            *buffer_ref.spec(),
        );
        buffer.copy_interleaved_ref(buffer_ref);

        for f in 0..frames {
            let mut mono_sample = 0f32;
            for c in 0..channels {
                mono_sample += buffer.samples()[f + c];
            }
            mono_sample /= channels as f32;
            let current_bin = &mut self.result[self.frame_in_track / self.frames_per_bin];
            if mono_sample > current_bin.maximum {
                current_bin.maximum = mono_sample;
            } else if mono_sample < current_bin.minimum {
                current_bin.minimum = mono_sample;
            }
            self.frame_in_track += 1;
        }
    }

    pub fn result(&self) -> &WaveformAnalysis {
        &self.result
    }
}
