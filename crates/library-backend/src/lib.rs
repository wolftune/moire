use aoide_backend_embedded::storage::DatabaseConfig;
use aoide_desktop_app::Environment;

pub use aoide_backend_embedded::Result;

pub use aoide_core_api::Pagination;

pub mod collection;
pub mod media;
pub mod playlist;
pub mod track;

/// Stateless library backend.
///
/// Stateless in a sense that it does not manage any application state.
///
/// All use cases are implemented as free functions in separate modules
/// that accept a borrowed reference to a cheaply `Clone`able and
/// `Send`able handle as the first parameter.
pub struct Backend {
    environment: Environment,
}

impl Backend {
    /// Set up the runtime environment.
    pub fn commission(db_config: DatabaseConfig) -> anyhow::Result<Self> {
        let environment = Environment::commission(db_config)?;
        Ok(Self { environment })
    }

    /// Prepare for tear down.
    ///
    /// Rejects new requests. Pending requests could still proceed until
    /// finished.
    pub fn decommission(&self) {
        self.environment.decommission();
    }

    /// A handle for invoking operations.
    pub fn handle(&self) -> &Handle {
        self.environment.handle()
    }
}

pub type Handle = aoide_desktop_app::environment::Handle;
pub type WeakHandle = aoide_desktop_app::environment::WeakHandle;
