use aoide_backend_embedded::track as api;

use aoide_core::{
    collection::EntityUid as CollectionUid, media::content::ContentPath, track::Entity,
};

use crate::{Handle, Pagination, Result};

pub async fn search(
    handle: &Handle,
    collection_uid: CollectionUid,
    params: aoide_core_api::track::search::Params,
    pagination: Pagination,
) -> Result<Vec<Entity>> {
    api::search(handle.db_gatekeeper(), collection_uid, params, pagination).await
}

pub async fn replace_many_by_media_source_content_path<I>(
    handle: &Handle,
    collection_uid: CollectionUid,
    params: aoide_usecases::track::replace::Params,
    validated_track_iter: I,
) -> Result<aoide_core_api::track::replace::Summary>
where
    I: IntoIterator<Item = aoide_usecases::track::ValidatedInput> + Send + 'static,
{
    api::replace_many_by_media_source_content_path(
        handle.db_gatekeeper(),
        collection_uid,
        params,
        validated_track_iter,
    )
    .await
}

pub async fn import_and_replace_many_by_local_file_path<I>(
    handle: &Handle,
    collection_uid: CollectionUid,
    params: aoide_usecases::track::import_and_replace::Params,
    content_path_iter: I,
    expected_content_path_count: Option<usize>,
) -> Result<aoide_usecases::track::import_and_replace::Outcome>
where
    I: IntoIterator<Item = ContentPath> + Send + 'static,
{
    api::import_and_replace_many_by_local_file_path(
        handle.db_gatekeeper(),
        collection_uid,
        params,
        content_path_iter,
        expected_content_path_count,
    )
    .await
}
