use aoide_backend_embedded::collection as api;

use aoide_core::collection::{Collection, Entity, EntityHeader, EntityUid};

use aoide_core_api::{
    collection::{EntityWithSummary, LoadScope},
    Pagination,
};
use aoide_repo::collection::MediaSourceRootUrlFilter;

use crate::{Handle, Result};

pub async fn load_all_kinds(handle: &Handle) -> Result<Vec<String>> {
    api::load_all_kinds(handle.db_gatekeeper()).await
}

pub async fn load_all(
    handle: &Handle,
    kind: Option<String>,
    media_source_root_url: Option<MediaSourceRootUrlFilter>,
    load_scope: LoadScope,
    pagination: Option<Pagination>,
) -> Result<Vec<EntityWithSummary>> {
    api::load_all(
        handle.db_gatekeeper(),
        kind,
        media_source_root_url,
        load_scope,
        pagination,
    )
    .await
}

pub async fn create(handle: &Handle, new_collection: Collection) -> Result<Entity> {
    api::create(handle.db_gatekeeper(), new_collection).await
}

pub async fn update(
    handle: &Handle,
    entity_header: EntityHeader,
    modified_collection: Collection,
) -> Result<Entity> {
    api::update(handle.db_gatekeeper(), entity_header, modified_collection).await
}

pub async fn purge(handle: &Handle, entity_uid: EntityUid) -> Result<()> {
    api::purge(handle.db_gatekeeper(), entity_uid).await
}
