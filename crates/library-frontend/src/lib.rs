use std::{path::PathBuf, sync::Arc};

use aoide_core::{
    collection::EntityUid as CollectionUid,
    track::{Entity as TrackEntity, EntityUid as TrackUid},
};
use aoide_core_api::media::source::ResolveUrlFromContentPath;
use aoide_desktop_app::{collection, settings, track};

pub(crate) use moire_library_backend::{Handle as BackendHandle, WeakHandle as WeakBackendHandle};

pub mod callback;
pub mod search;

const NESTED_MUSIC_DIRS: collection::NestedMusicDirectoriesStrategy =
    collection::NestedMusicDirectoriesStrategy::Permit;

// We always need the URL in addition to the virtual file path
const RESOLVE_TRACK_URL_FROM_CONTENT_PATH: Option<ResolveUrlFromContentPath> =
    Some(ResolveUrlFromContentPath::CanonicalRootUrl);

fn default_track_search_params() -> aoide_core_api::track::search::Params {
    aoide_core_api::track::search::Params {
        resolve_url_from_content_path: RESOLVE_TRACK_URL_FROM_CONTENT_PATH.clone(),
        ..Default::default()
    }
}

const TRACK_REPO_SEARCH_PREFETCH_LIMIT: usize = 100;

/// Stateful library frontend.
///
/// Manages the application state that should not depend on any
/// particular UI technology.
pub struct Frontend {
    settings_state: Arc<settings::ObservableState>,
    collection_state: Arc<collection::ObservableState>,
    track_search_state: Arc<track::repo_search::ObservableState>,
}

impl Frontend {
    #[must_use]
    pub fn spawn(
        settings_dir: PathBuf,
        tokio_rt: &tokio::runtime::Handle,
        backend: &BackendHandle,
    ) -> Self {
        let initial_track_search_state =
            track::repo_search::State::new(default_track_search_params());
        let this = Self {
            settings_state: Default::default(),
            collection_state: Default::default(),
            track_search_state: Arc::new(track::repo_search::ObservableState::new(
                initial_track_search_state,
            )),
        };
        // Spawn tasklets
        tokio_rt.spawn(settings::tasklet::on_state_changed_save_to_file(
            this.settings_state.subscribe(),
            settings_dir,
            |err| {
                log::error!("Failed to save settings to file: {err}");
            },
        ));
        tokio_rt.spawn(collection::tasklet::on_settings_changed(
            Arc::downgrade(&this.settings_state),
            Arc::downgrade(&this.collection_state),
            backend.downgrade(),
            NESTED_MUSIC_DIRS,
            |err| {
                log::error!("Failed to update collection after music directory changed: {err}");
            },
        ));
        tokio_rt.spawn(track::repo_search::tasklet::on_collection_changed(
            Arc::downgrade(&this.collection_state),
            Arc::downgrade(&this.track_search_state),
        ));
        tokio_rt.spawn(track::repo_search::tasklet::on_should_prefetch(
            Arc::downgrade(&this.track_search_state),
            backend.downgrade(),
            Some(TRACK_REPO_SEARCH_PREFETCH_LIMIT),
        ));
        this
    }

    /// Observable settings state.
    #[must_use]
    pub fn settings_state(&self) -> &Arc<settings::ObservableState> {
        &self.settings_state
    }

    /// Observable collection state.
    #[must_use]
    pub fn collection_state(&self) -> &Arc<collection::ObservableState> {
        &self.collection_state
    }

    /// Observable track (repo) search state.
    #[must_use]
    pub fn track_search_state(&self) -> &Arc<track::repo_search::ObservableState> {
        &self.track_search_state
    }

    /// Load multiple track entities from the current collection.
    pub async fn load_tracks_by_uid(
        &self,
        backend: &BackendHandle,
        track_uids: Vec<TrackUid>,
    ) -> anyhow::Result<Vec<TrackEntity>> {
        let collection_uid =
            if let Some(collection_uid) = self.collection_state().read().entity_uid() {
                collection_uid.clone()
            } else {
                anyhow::bail!("No collection");
            };
        self::load_tracks_by_uid(backend, collection_uid, track_uids).await
    }
}

/// Load multiple track entities from the given collection.
pub async fn load_tracks_by_uid(
    backend: &BackendHandle,
    collection_uid: CollectionUid,
    track_uids: Vec<TrackUid>,
) -> anyhow::Result<Vec<TrackEntity>> {
    let params = aoide_core_api::track::search::Params {
        filter: Some(aoide_core_api::track::search::Filter::AnyTrackUid(
            track_uids,
        )),
        ..default_track_search_params()
    };
    let pagination = Default::default();
    let entities =
        moire_library_backend::track::search(backend, collection_uid, params, pagination).await?;
    Ok(entities)
}
