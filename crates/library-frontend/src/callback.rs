//! Synchronous callbacks
//!
//! Could be attached to UI controls.

use std::{
    future::Future,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};

use aoide_desktop_app::{collection, fs::choose_directory};

use crate::{
    search::parse_track_search_filter_from_input, BackendHandle, Frontend, WeakBackendHandle,
    NESTED_MUSIC_DIRS,
};

// Simplified, local state management. A more sophisticated solution
// that enables/disables the corresponding buttons accordingly would
// require an application-wide global state management.
static ON_CHOOSE_MUSIC_DIR_GUARD: AtomicBool = AtomicBool::new(false);

pub fn on_music_dir_choose(tokio_rt: tokio::runtime::Handle, frontend: &Frontend) -> impl FnMut() {
    let settings_state = Arc::clone(frontend.settings_state());
    move || {
        if ON_CHOOSE_MUSIC_DIR_GUARD
            .compare_exchange(false, true, Ordering::Acquire, Ordering::Relaxed)
            .unwrap_or(true)
        {
            // Prevent opening multiple file dialogs at the same time
            log::debug!("Already choosing a music directory");
            return;
        }
        log::info!("Choosing music directory...");
        let settings_state = Arc::clone(&settings_state);
        tokio_rt.spawn(async move {
            let old_music_dir = settings_state.read().music_dir.clone();
            let new_music_dir = choose_directory(old_music_dir.as_deref()).await;
            if let Some(music_dir) = &new_music_dir {
                log::info!("Updating music directory: {}", music_dir.display());
                settings_state.update_music_dir(music_dir);
            }
            ON_CHOOSE_MUSIC_DIR_GUARD
                .compare_exchange(true, false, Ordering::Release, Ordering::Relaxed)
                .expect("infallible");
        });
    }
}

pub fn on_music_dir_reset(frontend: &Frontend) -> impl FnMut() {
    let settings_state = Arc::clone(frontend.settings_state());
    move || {
        log::info!("Resetting music directory");
        settings_state.reset_music_dir();
    }
}

pub fn on_collection_rescan_music_dir(
    tokio_rt: tokio::runtime::Handle,
    backend: WeakBackendHandle,
    frontend: &Frontend,
) -> impl FnMut() {
    let collection_state = Arc::downgrade(frontend.collection_state());
    let track_search_state = Arc::downgrade(frontend.track_search_state());
    move || {
        let report_progress_fn =
            |progress: Option<aoide_backend_embedded::batch::rescan_collection_vfs::Progress>| {
                // TODO: The reporting probably needs to be debounced, otherwise it
                // could overload the UI. The synchronous invocation will also slow
                // down the batch task if it takes too much time.
                if let Some(progress) = progress {
                    log::warn!("TODO Report progress while rescanning collection: {progress:?}");
                } else {
                    log::warn!("TODO Report rescanning collection finished");
                }
            };
        let collection_state = if let Some(collection_state) = collection_state.upgrade() {
            collection_state
        } else {
            return;
        };
        let backend = if let Some(backend) = backend.upgrade() {
            backend
        } else {
            return;
        };
        let task = collection_rescan_music_dir_task(collection_state, backend, report_progress_fn);
        let track_search_state = track_search_state.clone();
        tokio_rt.spawn(async move {
            match task.await {
                Ok(outcome) => {
                    log::warn!("TODO Report outcome after rescanning collection: {outcome:?}");
                    // Discard any cached search results
                    if let Some(track_search_state) = track_search_state.upgrade() {
                        track_search_state.reset_fetched();
                    }
                }
                Err(err) => {
                    log::warn!("TODO Report error after rescanning collection: {err}");
                }
            }
        });
    }
}

fn collection_rescan_music_dir_task(
    collection_state: Arc<collection::ObservableState>,
    backend: BackendHandle,
    mut report_progress_fn: impl FnMut(Option<aoide_backend_embedded::batch::rescan_collection_vfs::Progress>)
        + Clone
        + Send
        + 'static,
) -> impl Future<
    Output = anyhow::Result<aoide_backend_embedded::batch::rescan_collection_vfs::Outcome>,
> + Send
       + 'static {
    let mut collection_uid = None;
    collection_state.modify(|state| {
        collection_uid = state.entity_uid().map(ToOwned::to_owned);
        collection_uid.is_some() && state.reset_to_pending()
    });
    async move {
        let collection_uid = if let Some(collection_uid) = collection_uid {
            collection_uid
        } else {
            anyhow::bail!("No collection");
        };
        log::debug!("Rescanning collection...");
        let res = {
            let mut report_progress_fn = report_progress_fn.clone();
            let report_progress_fn = move |progress| {
                report_progress_fn(Some(progress));
            };
            collection::rescan_vfs(&backend, collection_uid, report_progress_fn).await
        };
        report_progress_fn(None);
        log::debug!("Rescanning collection finished: {:?}", res);
        if let Err(err) = collection_state
            .refresh_from_db(&backend, NESTED_MUSIC_DIRS)
            .await
        {
            log::warn!("Failed to refresh collection after rescanning music directory: {err}");
        }
        res
    }
}

pub fn on_track_search_submit_input(frontend: &Frontend) -> impl FnMut(String) -> String {
    let track_search_state = Arc::clone(frontend.track_search_state());
    move |input| {
        log::debug!("Received search input: {input}");
        let input = input.trim().to_owned();
        let filter = parse_track_search_filter_from_input(&input);
        let resolve_url_from_content_path = track_search_state
            .read()
            .default_params()
            .resolve_url_from_content_path
            .clone();
        let mut params = aoide_core_api::track::search::Params {
            filter,
            ordering: vec![], // TODO
            resolve_url_from_content_path,
        };
        // Argument is consumed when updating succeeds
        if !track_search_state.update_params(&mut params) {
            log::debug!("Track search params not updated: {params:?}");
        }
        input
    }
}
