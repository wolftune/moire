use basedrop::Owned;

use crate::{ramping_value::RampingValue, Clip};

/// Decks handle one audio track on the timeline. A Deck
/// can contain an arbitrary number of Clips.
pub struct Deck {
    clips: Option<Owned<Vec<Clip>>>,
    pub buffer_main_out: Vec<Vec<f32>>,
    pub buffer_headphones_out: Vec<Vec<f32>>,
    pub volume: RampingValue,
    pub headphones_enabled: RampingValue,
}

impl Default for Deck {
    fn default() -> Self {
        Self {
            clips: None,
            buffer_main_out: vec![Vec::<f32>::new(); 2],
            buffer_headphones_out: vec![Vec::<f32>::new(); 2],
            volume: RampingValue::new(1.0),
            headphones_enabled: RampingValue::new(0.0),
        }
    }
}

impl Deck {
    pub fn load_clip(&mut self, new_clip: Clip, mut new_vec: Owned<Vec<Clip>>) {
        // new_vec has a capacity enough for the old clips plus new_clip without
        // allocating on push, so move the old clips into new_vec.
        if let Some(clips) = &mut self.clips {
            for _ in 0..clips.len() {
                new_vec.push(clips.pop().unwrap());
            }
        }
        new_vec.push(new_clip);
        new_vec.sort_unstable_by(|a, b| {
            a.start_on_timeline_frames
                .partial_cmp(&b.start_on_timeline_frames)
                .unwrap()
        });
        self.clips = Some(new_vec);
    }

    pub fn seek_clip(&mut self, clip_index: usize, diff_seconds: f32) {
        self.clips.as_mut().unwrap()[clip_index].seek(diff_seconds);
    }

    pub fn process(&mut self, playhead_main_frames: f32, headphones_playhead_frames: f32) {
        let mut any_clips_processed = false;
        if let Some(clips) = &mut self.clips {
            for clip in clips.iter_mut() {
                let clip_processed = clip.process(
                    &mut self.buffer_main_out,
                    playhead_main_frames,
                    &mut self.buffer_headphones_out,
                    headphones_playhead_frames,
                );
                if !any_clips_processed && clip_processed {
                    any_clips_processed = true;
                }
            }
        }
        if !any_clips_processed {
            for channel in self.buffer_main_out.iter_mut() {
                channel.fill(0f32);
            }
            for channel in self.buffer_headphones_out.iter_mut() {
                channel.fill(0f32);
            }
        }

        ramp_gain(
            &mut self.buffer_headphones_out,
            &mut self.headphones_enabled,
        );

        ramp_gain(&mut self.buffer_main_out, &mut self.volume);
    }

    pub fn set_tempo_ratio(&mut self, tempo_ratio: f32) {
        if let Some(clips) = &mut self.clips {
            for clip in clips.iter_mut() {
                clip.set_tempo_ratio(tempo_ratio);
            }
        }
    }

    pub fn set_buffer_size(&mut self, buffer_size_frames: usize) {
        for channel in &mut self.buffer_main_out {
            channel.resize(buffer_size_frames, 0f32);
        }
        for channel in &mut self.buffer_headphones_out {
            channel.resize(buffer_size_frames, 0f32);
        }
        self.volume.steps = buffer_size_frames;
        self.headphones_enabled.steps = buffer_size_frames;

        if let Some(clips) = &mut self.clips {
            for clip in clips.iter_mut() {
                clip.set_buffer_size(buffer_size_frames);
            }
        }
    }

    pub fn set_sample_rate(&mut self, sample_rate: usize) {
        if let Some(clips) = &mut self.clips {
            for clip in clips.iter_mut() {
                clip.set_sample_rate(sample_rate);
            }
        }
    }
}

fn ramp_gain(buffer: &mut [Vec<f32>], gain: &mut RampingValue) {
    for (frame, gain) in (0..buffer[0].len()).zip(gain) {
        for channel in buffer.iter_mut() {
            channel[frame] *= gain;
        }
    }
}
