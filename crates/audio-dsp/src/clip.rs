use creek::{ReadDiskStream, ReadStreamOptions, SeekMode, SymphoniaDecoder};

use log::error;
use rubato::{InterpolationParameters, InterpolationType, Resampler, SincFixedOut, WindowFunction};

/// Clip handles one (segment of an) audio file within a deck.
pub struct Clip {
    pub start_on_timeline_frames: f32,
    pub start_in_file_frames: f32,
    pub duration_frames: f32,
    pub file_sample_rate: usize,
    engine_sample_rate: usize,
    buffer_size_frames: usize,
    tempo_ratio: f32,
    resampler_input_buffer: Vec<Vec<f32>>,
    read_stream_main: ReadDiskStream<SymphoniaDecoder>,
    read_stream_headphones: ReadDiskStream<SymphoniaDecoder>,
    resampler_main: SincFixedOut<f32>,
    resampler_headphones: SincFixedOut<f32>,
}

fn make_resampler(base_ratio: f64, buffer_size_frames: usize) -> SincFixedOut<f32> {
    let params = InterpolationParameters {
        sinc_len: 256,
        f_cutoff: 0.95,
        oversampling_factor: 128,
        interpolation: InterpolationType::Cubic,
        window: WindowFunction::Hann2,
    };
    SincFixedOut::new(base_ratio, 10.0, params, buffer_size_frames, 2).unwrap()
}

impl Clip {
    pub fn new(
        engine_sample_rate: usize,
        buffer_size_frames: usize,
        file_path: &std::path::Path,
        start_on_timeline_seconds: f32,
        start_in_file_seconds: f32,
        end_in_file_seconds: Option<f32>,
    ) -> Clip {
        let opts = ReadStreamOptions {
            num_cache_blocks: 20,
            ..Default::default()
        };
        let mut read_stream_main =
            ReadDiskStream::<SymphoniaDecoder>::new(file_path, 0, opts).unwrap();

        let opts = ReadStreamOptions {
            num_cache_blocks: 20,
            ..Default::default()
        };
        let mut read_stream_headphones =
            ReadDiskStream::<SymphoniaDecoder>::new(file_path, 0, opts).unwrap();

        let file_info = read_stream_main.info();

        let file_sample_rate = file_info.sample_rate.unwrap() as f32;

        let resampler_main = make_resampler(
            engine_sample_rate as f64 / file_sample_rate as f64,
            buffer_size_frames,
        );
        let resampler_headphones = make_resampler(
            engine_sample_rate as f64 / file_sample_rate as f64,
            buffer_size_frames,
        );

        let start_on_timeline_frames = start_on_timeline_seconds * file_sample_rate;
        let start_in_file_frames = start_in_file_seconds * file_sample_rate;
        let duration_frames = match end_in_file_seconds {
            Some(duration_seconds) => duration_seconds * file_sample_rate,
            None => file_info.num_frames as f32,
        };

        // Cache the start of the file into cache with index `0`.
        let _ = read_stream_main.cache(0, start_in_file_frames as usize);
        let _ = read_stream_headphones.cache(0, start_in_file_frames as usize);

        // Tell the stream to seek to the beginning of file. This will also alert the stream to the existence
        // of the cache with index `0`.
        read_stream_main
            .seek(start_in_file_frames as usize, Default::default())
            .unwrap();
        read_stream_headphones
            .seek(start_in_file_frames as usize, Default::default())
            .unwrap();

        // Wait until the buffer is filled before sending it to the process thread.
        read_stream_main.block_until_ready().unwrap();
        read_stream_headphones.block_until_ready().unwrap();

        Clip {
            start_on_timeline_frames,
            start_in_file_frames,
            duration_frames,
            file_sample_rate: file_sample_rate as usize,
            engine_sample_rate,
            buffer_size_frames,
            tempo_ratio: 1.0,
            resampler_input_buffer: resampler_main.input_buffer_allocate(),
            read_stream_main,
            read_stream_headphones,
            resampler_main,
            resampler_headphones,
        }
    }

    pub fn set_tempo_ratio(&mut self, tempo_ratio: f32) {
        self.resampler_main
            .set_resample_ratio_relative(tempo_ratio as f64)
            .unwrap();
        self.resampler_headphones
            .set_resample_ratio_relative(tempo_ratio as f64)
            .unwrap();
        self.tempo_ratio = tempo_ratio;
    }

    pub fn set_buffer_size(&mut self, buffer_size_frames: usize) {
        self.buffer_size_frames = buffer_size_frames;
        self.resampler_main = make_resampler(
            self.engine_sample_rate as f64 / self.file_sample_rate as f64,
            self.buffer_size_frames,
        );
        self.resampler_headphones = make_resampler(
            self.engine_sample_rate as f64 / self.file_sample_rate as f64,
            self.buffer_size_frames,
        );
    }

    pub fn set_sample_rate(&mut self, engine_sample_rate: usize) {
        self.engine_sample_rate = engine_sample_rate;
        self.resampler_main = make_resampler(
            self.engine_sample_rate as f64 / self.file_sample_rate as f64,
            self.buffer_size_frames,
        );
        self.resampler_headphones = make_resampler(
            self.engine_sample_rate as f64 / self.file_sample_rate as f64,
            self.buffer_size_frames,
        );
    }

    pub fn seek(&mut self, diff_seconds: f32) {
        let diff_frames = diff_seconds * self.file_sample_rate as f32;
        self.start_on_timeline_frames -= diff_frames;

        let main_playhead: usize = (self.read_stream_main.playhead() as isize
            + diff_frames as isize)
            .try_into()
            .unwrap_or(0);
        self.read_stream_main
            .seek(main_playhead, SeekMode::Auto)
            .unwrap();

        let headphones_playhead: usize = (self.read_stream_headphones.playhead() as isize
            + diff_frames as isize)
            .try_into()
            .unwrap_or(0);
        self.read_stream_headphones
            .seek(headphones_playhead, SeekMode::Auto)
            .unwrap();
    }

    pub fn process(
        &mut self,
        buffer_main: &mut Vec<Vec<f32>>,
        first_frame_main: f32,
        buffer_headphones: &mut Vec<Vec<f32>>,
        first_frame_headphones: f32,
    ) -> bool {
        let main_processed = process_inner(
            self.start_on_timeline_frames,
            self.duration_frames * self.tempo_ratio,
            &mut self.resampler_input_buffer,
            buffer_main,
            &mut self.read_stream_main,
            &mut self.resampler_main,
            first_frame_main,
        );
        let headphones_processed = process_inner(
            self.start_on_timeline_frames,
            self.duration_frames * self.tempo_ratio,
            &mut self.resampler_input_buffer,
            buffer_headphones,
            &mut self.read_stream_headphones,
            &mut self.resampler_headphones,
            first_frame_headphones,
        );
        main_processed || headphones_processed
    }
}

fn process_inner(
    start_on_timeline_frames: f32,
    duration_frames: f32,
    resampler_input_buffer: &mut [Vec<f32>],
    buffer: &mut Vec<Vec<f32>>,
    read_stream: &mut ReadDiskStream<SymphoniaDecoder>,
    resampler: &mut SincFixedOut<f32>,
    first_frame: f32,
) -> bool {
    if first_frame < start_on_timeline_frames
        || first_frame > start_on_timeline_frames + duration_frames
    {
        return false;
    }
    let creek_channels = read_stream.info().num_channels as usize;
    match read_stream.read(resampler.input_frames_next()) {
        Ok(read_data) => {
            // Usually, creek returns the number of frames requested and they can be
            // passed directly to rubato without a copy. However, at the end of the
            // file, creek returns less than the number of requested frames. In this case,
            // rubato still needs its specified amount of input frames, so copy the frames
            // from creek into an intermediate buffer.
            if read_data.read_channel(0).len() >= resampler.input_frames_next() {
                let input;
                if creek_channels == 1 {
                    input = [read_data.read_channel(0), read_data.read_channel(0)];
                    resampler
                        .process_into_buffer(&input, buffer, Some(&[true, false]))
                        .unwrap();

                    // The borrow checker doesn't approve simply using copy_from_slice
                    // from one channel of the buffer to another.
                    if let [left, right] = &mut buffer[..] {
                        right.copy_from_slice(left);
                    } else {
                        unreachable!("resampler_input_buffer is not a stereo buffer");
                    }
                } else {
                    input = [read_data.read_channel(0), read_data.read_channel(1)];
                    resampler.process_into_buffer(&input, buffer, None).unwrap();
                }
            } else if creek_channels == 1 {
                let creek_channel = read_data.read_channel(0);
                resampler_input_buffer[0].fill(0.0);
                resampler_input_buffer[0][..creek_channel.len()].copy_from_slice(creek_channel);

                resampler
                    .process_into_buffer(resampler_input_buffer, buffer, Some(&[true, false]))
                    .unwrap();

                // The borrow checker doesn't approve simply using copy_from_slice
                // from one channel of the buffer to another.
                if let [left, right] = &mut resampler_input_buffer[..] {
                    right.copy_from_slice(left);
                } else {
                    unreachable!("resampler_input_buffer is not a stereo buffer");
                }
            } else {
                for (channel_index, channel) in resampler_input_buffer.iter_mut().enumerate() {
                    channel.fill(0.0);
                    let creek_channel = read_data.read_channel(channel_index);
                    channel[..creek_channel.len()].copy_from_slice(creek_channel);
                }
                resampler
                    .process_into_buffer(resampler_input_buffer, buffer, None)
                    .unwrap();
            }
        }
        Err(e) => {
            match e {
                // This can happen when seeking near the end of the file
                creek::read::ReadError::EndOfFile => {}
                _ => error!("creek read error! {e:?}"),
            }
            for output_channel in buffer {
                output_channel.fill(0f32);
            }
        }
    }
    true
}
